<?php
/**
 * @file
 * Basic page generation functions and forms constructors.
 */

/**
 * This function calls in menu hook. It show the list of all submissions.
 *
 * @see formsave_menu()
 */
function _formsave_page_show_list() {
  // define table header
  $tb_head = array();
  $tb_head[] = array('data' => t('SID'), 'field' => 'sid', 'title' => t('Submission ID'));
  $tb_head[] = array('data' => t('Form ID'), 'field' => 'fid', 'title' => t('Form ID'));
  $tb_head[] = array('data' => t('Date'), 'field' => 'date', 'sort' => 'desc', 'title' => t('Submission date'));

  // query
  $sql = "SELECT fid, sid, value AS `date` FROM {formsave} WHERE field = '#submitted' ";

  // append filter
  $session = &$_SESSION['formsave_filter'];
  $session = is_array($session) ? $session : array();
  if (!empty($session)) {
    $sql .= ' AND fid IN (' . str_repeat("'%s', ", count($session));
    $sql = drupal_substr($sql, 0, -2); //remove the extra symbols ", " from sql
    $sql .= ')';
  }

  // sorting results
  $sql .= tablesort_sql($tb_head);

  // execute query
  if (empty($session)) {
    $res = pager_query($sql, 50, 0, NULL);
  }
  else {
    $res = pager_query($sql, 50, 0, NULL, $session);
  }

  $tb_body = array();
  while ($row = db_fetch_object($res)) {
    $tb_row = array();
    $tb_row[] = array(
      'data' => l($row->sid, 'admin/content/formsave/'. $row->sid), 'class' => 'formsave-sid',
    );
    $tb_row[] = array(
      'data' => $row->fid, 'class' => 'formsave-fid',
    );
    $tb_row[] = array(
      'data' => date('d.m.Y H:i:s', $row->date), 'class' => 'formsave-date',
    );
    $tb_body[] = $tb_row;
  }

  // render filter form
  $str = drupal_get_form('formsave_filter_form');

  // render table
  $str .= theme('table', $tb_head, $tb_body);

  // render paginator
  $str .= theme('pager');

  return $str;
}

/**
 * This function calls in menu hook. It show the particular submission.
 *
 * @see formsave_menu()
 */
function _formsave_page_show_item($sid = NULL) {
  $str = formsave_show_submission($sid);
  if (!$str) {
    return t('No submission');
  }
  return $str;
}


/************ FORMS ***********/

/**
 * Form builder for the FormSave submissions list filter.
 *
 * @see formsave_filter_form_submit()
 * @ingroup forms
 */
function formsave_filter_form(&$form_state) {
  // read the filter settings from session
  $session = &$_SESSION['formsave_filter'];
  $session = is_array($session) ? $session : array();

  // generate the options array
  $result = db_query("SELECT DISTINCT fid FROM {formsave}");
  $options = array();
  while ($row = db_fetch_object($result)) {
    $options[$row->fid] = $row->fid;
  }

  $form = array();

  $form['formsave_filter'] = array(
    '#type' => 'fieldset',
    '#title' => t('Filter'),
    '#collapsible' => TRUE,
    '#collapsed' => empty($session),
  );

  $form['formsave_filter']['fids'] = array(
    '#type' => 'select',
    '#title' => t('Form ID'),
    '#options' => $options,
    '#multiple' => TRUE,
  );

  if (!empty($session)) {
    $form['formsave_filter']['fids']['#default_value'] = $session;
  }

  $form['formsave_filter']['apply'] = array(
    '#type' => 'submit',
    '#value' => t('Filter'),
  );

  if (!empty($session)) {
    $form['formsave_filter']['reset'] = array(
      '#type' => 'submit',
      '#value' => t('Reset'),
    );
  }

  return $form;
}

/**
 * Validate result from formsave filter form.
 *
 * @see formsave_filter_form()
 * @see formsave_filter_form_submit()
 * @ingroup forms
 */
function formsave_filter_form_validate($form, &$form_state) {
  if ($form_state['values']['op'] == t('Filter') && empty($form_state['values']['fids'])) {
    form_set_error('fids', t('You must select something to filter by.'));
  }
}

/**
 * Form submission handler for formsave_filter_form().
 *
 * @see formsave_filter_form()
 * @see formsave_filter_form_validate()
 * @ingroup forms
 */
function formsave_filter_form_submit($form, &$form_state) {
  $session = &$_SESSION['formsave_filter'];
  switch ($form_state['values']['op']) {
    case t('Filter'):
      $session = $form_state['values']['fids'];
      break;
    case t('Reset'):
      $session = array();
      break;
  }
}

/**
 * Form builder for the FormSave administration form.
 *
 * @see formsave_admin_form_submit()
 * @ingroup forms
 */
function formsave_admin_form(&$form_state) {
  $form = array();
  $form['formsave_formids'] = array(
    '#type' => 'textarea',
    '#title' => t('Form id'),
    '#description' => t('Enter the form identificators to save them submissions, one id per row.'),
    '#default_value' => implode("\n", variable_get('formsave_formids', array())) . "\n",
    '#required' => TRUE,
    '#weight' => -10,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );

  return $form;
}

/**
 * Form submission handler for formsave_admin_form().
 *
 * @see formsave_admin_form()
 * @ingroup forms
 */
function formsave_admin_form_submit($form, &$form_state) {
  $formids = $form_state['values']['formsave_formids'];
  $formids = explode("\n", $formids);
  $formids = array_map('trim', $formids);
  $formids = array_unique($formids);
  $formids = array_filter($formids); // delete empty elements from array
  variable_set('formsave_formids', $formids);
}
